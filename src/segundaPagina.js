import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useEffect, useState } from "react";

export default function SegundaPagina() {
    const [count, setCount] = useState(0); 

  return (
    <View style={styles.container}>
      <ScrollView>

        <Text>Click para contar: {count}</Text>

        <TouchableOpacity
          style={styles.teste}
          onPress={() => setCount(count + 1)}
        >
          <Text>Click</Text>
        </TouchableOpacity>
      </ScrollView>
      {/* <StatusBar style="auto" /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#FFC0CB",
    alignItems: "center",
    justifyContent: "center",
  },

  teste: {
    color: "#FFC0CB",
    width: 90,
    borderRadius: 25,
    alignItems: "center",
    marginTop: 7,
  },
});


