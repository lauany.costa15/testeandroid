import { StatusBar } from "expo-status-bar";
import {
  StyleSheet,
  Text,
  View,
  Button,
  TouchableOpacity,
  ScrollView,
} from "react-native";
import { useEffect, useState } from "react";

export default function QuintaPagina() {
  return (
    <View style={styles.container}>
      <ScrollView>
      <TouchableOpacity
          style={styles.teste}
        >
          <Text>Click</Text>
        </TouchableOpacity>
      </ScrollView>
      {/* <StatusBar style="auto" /> */}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#C2DBF0",
    alignItems: "center",
    justifyContent: "center",
  },

  teste: {
    color: "black",
    backgroundColor: "#97A4E5",
    width: 90,
    borderRadius: 25,
    alignItems: "center",
    marginTop: 7,
  },
});
