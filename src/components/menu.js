import {
  StyleSheet,
  Text,
  View,
  ScrollView,
  TouchableOpacity,
} from "react-native";
import { StatusBar } from "expo-status-bar";


export default function Menu({navigation}) {
  return (
    <View style={styles.container}>
      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("PrimeiraPagina")}
      >
        <Text>Contagem de caracteres</Text>
      </TouchableOpacity>

      <TouchableOpacity
        style={styles.menu}
        onPress={() => navigation.navigate("SegundaPagina")}
        >
        <Text>Contagem de cliques</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
      onPress={() => navigation.navigate("TerceiraPagina")}
      >
        <Text>Lista de textos</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu} 
      onPress={() => navigation.navigate("QuartaPagina")}
      >
        <Text>Saudações em diferentes idiomas</Text>
      </TouchableOpacity>

      <TouchableOpacity style={styles.menu}
      onPress={() => navigation.navigate("QuintaPagina")}
      >
        <Text>Inatividade</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: "column",
    justifyContent: "center",
    alignItems: "center",
  },
  Menu: {
    padding: 10,
    margin: 5,
    backgroundColor: "pink",
    borderRadius: 5,
  },
});
