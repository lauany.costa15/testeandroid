import { StatusBar } from 'expo-status-bar';
import { createStackNavigator } from "@react-navigation/stack";
import { NavigationContainer } from "@react-navigation/native";
import { StyleSheet, Text, View } from 'react-native';
import TelaInicial from "./src/telaInicial";

import SegundaPagina from './src/segundaPagina';
import TerceiraPagina from './src/terceiraPagina';
import QuartaPagina from './src/quartaPagina';
import QuintaPagina from './src/quintaPagina';
import Menu from './src/components/menu';



const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator>
      <Stack.Screen name="Menu" component={Menu}/>
      <Stack.Screen name="TelaInicial" component={TelaInicial}/>
      <Stack.Screen name="SegundaPagina" component={SegundaPagina}/>
        <Stack.Screen name="TerceiraPagina" component={TerceiraPagina}/>
        <Stack.Screen name="QuartaPagina" component={QuartaPagina}/>
        <Stack.Screen name="QuintaPagina" component={QuintaPagina}/>
        
        
      </Stack.Navigator>
    </NavigationContainer>
  );
}


  
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
